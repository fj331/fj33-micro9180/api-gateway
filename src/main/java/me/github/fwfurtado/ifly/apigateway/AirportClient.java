package me.github.fwfurtado.ifly.apigateway;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name = "airports")
public interface AirportClient {

    @GetMapping("/actuator/info")
    Map<String, Object> info();
}
