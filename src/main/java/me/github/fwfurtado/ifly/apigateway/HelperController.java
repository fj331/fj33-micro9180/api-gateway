package me.github.fwfurtado.ifly.apigateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RestController
public class HelperController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AirportClient client;

    @GetMapping("info")
    String showInfo() {

        return restTemplate.getForObject("http://airports/actuator/info", String.class);
    }

    @GetMapping("info2")
    Map<String, Object> info2() {
        return client.info();
    }

}
